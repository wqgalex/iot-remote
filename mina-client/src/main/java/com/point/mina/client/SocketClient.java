package com.point.mina.client;

import java.net.InetSocketAddress;
import java.util.Arrays;

import org.apache.log4j.Logger;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.future.ConnectFuture;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.transport.socket.nio.NioSocketConnector;

import com.point.iot.base.message.PointMessage;

/**
 * 客户端
 * @author LiuLiu
 *
 */
public class SocketClient implements Runnable {
	Logger logger = Logger.getLogger(SocketClient.class);
	private NioSocketConnector mConnector;
	private ConnectFuture mFuture;
	//服务器地址
	private String mAddr;
	//服务器端口
	private int mPort;
	//
	private ClientHanlder mClientHandler;

	public SocketClient(String addr, int port) {
		this(addr, port, null, 0);
		logger.info("MinaSocketClient初始化 " + addr + ":" + port);
	}
	
	/**
	 * 构造函数
	 * @param addr 地址
	 * @param port 端口
	 * @param msgHandler 消息处理类
	 * @param handShakeMsgID 握手消息ID
	 */
	public SocketClient(String addr, int port, ClientHanlder msgHandler, int handShakeMsgID) {
		mAddr = addr;
		mPort = port;
		this.mClientHandler = msgHandler;
		connect(true);
	}
	public String getmAddr() {
		return mAddr;
	}
	public void setmAddr(String mAddr) {
		this.mAddr = mAddr;
	}
	/**
	 * 构造函数
	 * @param addr 地址
	 * @param port 端口
	 * @param msgHandler 消息处理类
	 * @param handShakeMsgID 握手消息ID
	 * @param bIsReconnect  是否需要重连
	 */
	public SocketClient(String addr, int port, int handShakeMsgID, boolean bIsReconnect) {
		mAddr = addr;
		mPort = port;
		connect(bIsReconnect);
	}
	
	/**
	 * 重置ConnectFuture，为重连做准备
	 */
	public void reset() {
		if (mFuture != null) {
			mFuture.cancel();
			mFuture = null;
		}
	}
	public void send(IoBuffer buf) {
		IoSession session = mFuture.getSession();
		session.write(buf);
	}
	public void send(PointMessage message) {
		IoSession session = mFuture.getSession();
		session.write(message);
	}
	/**
	 * 开始连接
	 * @throws InterruptedException 
	 */
	private synchronized void connect (boolean bIsReconnect) {
		connect(bIsReconnect, false);
	}
	
	
	private synchronized void connect (boolean bIsReconnect, boolean bUseReadOperation) {
		try {
			if (mFuture != null && mFuture.isConnected()) {
				return;
			}
			if (mPort == 0) {
				return;
			}
			mConnector = new NioSocketConnector();
			//设置连接超时时间
			mConnector.setConnectTimeoutMillis(2000);
			mConnector.getFilterChain().addLast("codec", new ProtocolCodecFilter(new MessageCodecFactory()));
			mConnector.setHandler(mClientHandler);
	//		logger.info("尝试连接 Server " + mAddr + ":" + mPort+"--------------------------------------------");
			mFuture = mConnector.connect(new InetSocketAddress(mAddr, mPort));
			//设置IDLE时间
			mConnector.getSessionConfig().setIdleTime(IdleStatus.READER_IDLE, 10);
			mConnector.getSessionConfig().setTcpNoDelay(false);
			mConnector.getSessionConfig().setUseReadOperation(bUseReadOperation);
			mFuture.awaitUninterruptibly();
			boolean connected=mFuture.isConnected();
			if (!connected && bIsReconnect) {
	//			logger.info("connect fail mFuture=[ " + mFuture + "],mAddr =[" + mAddr + ":" + mPort);
				reset();
				mConnector.dispose(true);
				//如果链接失败，则重新启动链接重试线程
				(new Thread(this)).start();
			} else {
				//如果连接成功，则发送所有队列中的消息
				logger.info("connect success " + mAddr + ":" + mPort);
			}
		} catch (Exception e) {
			logger.fatal("SocketClient ["+mAddr+"  :  "+ mPort+"] connect()失败    Exception: " + e + " " + Arrays.toString(e.getStackTrace()));
			//如果链接失败，则重新启动链接重试线程
			(new Thread(this)).start();
		}
	}
	
	/**
	 * 若链接失败，每N秒重试一次链接
	 */
	public void run() {
		try {
			Thread.sleep(10000);
		} catch (Exception e) {
		}
		connect(true);
	}
		
	/**
	 * 判断是否已经连接上
	 * @return
	 */
	public boolean isConnected(){
		if(mFuture !=null && mFuture.isConnected()){
			return true;
		}
		return false;
	}
	
	public boolean isActive(){
		return mConnector.isActive();
	}
	
	public void close(boolean immediately) {
		if(mFuture!=null){
			mFuture.getSession().closeNow();
			mFuture.getSession().getService().dispose();
		}
		if(mConnector!=null)mConnector.dispose();
	}
	
}
